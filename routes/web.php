<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/** Standard initial route for the app */
Route::get('/', function () {
    return view('welcome');
});

/** Stops the register, reset and verify routes from being accessed, usually appear within the standard Laravel UI/auth login page */
Auth::routes([

  'register' => false, // Register Routes...

  'reset' => false, // Reset Password Routes...

  'verify' => false, // Email Verification Routes...

]);

/** Route to access the back end admin section - must be logged in */
Route::any('/admin/{any?}', 'AdminController@index')->where('any', '.*')->middleware('auth');

/** Subsequent routes for pages past the initial home page if any exist */
Route::get('/{any?}', function ($any) {
    return view('welcome');
})->where('any', '.*');
