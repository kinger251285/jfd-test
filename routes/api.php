<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/** Get the current user info - i use this usually for user info within an admin panel e.g. displaying users name */
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/** API resource allows usual CRUD actions via GET, POST, PUT, DELETE requests to a specific controller - alternatively instead of using apiResource you can choose the request, route path and the specific controller along with a specific function */
Route::apiResource('locationDesperation', 'LocationDesperationController');
