<?php

namespace App\Http\Controllers;

use App\LocationDesperation;
use Illuminate\Http\Request;
use App\Http\Resources\LocationDesperationResource;
use Carbon\carbon;
use DB;

class LocationDesperationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        /** Get all the Desperation Rating Records that exist within the database */
        $LD = LocationDesperation::all();

        return LocationDesperationResource::collection($LD);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /** Validation for a 2nd time to check the must have fields have been filled & with a custom error message*/
        $request->validate([
                'location' => 'required',
                'desperation' => 'required',                

        ],
        [
                'location.required' => 'The form submission requires a location, Please add a location',
                'desperation.required' => 'The form submission requires a desperation rating, Please add a desperation rating',                

        ]);

        /** Get the form data */
        $Location = $request->get('location');
        $Comment = $request->get('comment');
        $Desperation = $request->get('desperation');

        /** Set an array for the form data and table fields ready for adding to the database */
        $data = array (
                'location' => $Location,
                'comment' => $Comment,  
                'desperation' => $Desperation,                                                                             
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            ); 

        LocationDesperation::insert($data);

        /** Custom response to provide feedback to the user */
        return response()->json('Thank you for submitting your opinion, your opinion has been captured successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LocationDesperation  $locationDesperation
     * @return \Illuminate\Http\Response
     */
    public function show(LocationDesperation $locationDesperation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LocationDesperation  $locationDesperation
     * @return \Illuminate\Http\Response
     */
    public function edit(LocationDesperation $locationDesperation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LocationDesperation  $locationDesperation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LocationDesperation $locationDesperation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LocationDesperation  $locationDesperation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /** Find and delete the specific row in the table */
        LocationDesperation::destroy($id);

        /** Custom user feedback message to let the user know the item has been deleted */
        return response()->json('Desperation Rating Successfully Deleted');
    }
}
