<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationDesperation extends Model
{
	/** Sets the fields that can be mass assigned - in the event of API usage */
    protected $fillable = ['location', 'comment', 'desperation'];
}
