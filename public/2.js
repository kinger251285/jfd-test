(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[2],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/HomeContent.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/HomeContent.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'HomeContent',
  data: function data() {
    return {
      rules: {
        required: function required(v) {
          return !!v || 'This field is required';
        },
        comment_count: function comment_count(value) {
          return (value || '').length <= 300 || 'Max 300 characters - please shorten your comment';
        }
      },
      form: false,
      locations: ['Cinema', 'Pub', 'Restaurant', 'Cleeve Hill Golf Course', 'Countryside', 'Swimming Pool'],
      userItem: {
        location: '',
        comment: '',
        desperation: ''
      },
      newErrors: [],
      errors: [],
      response: '',
      snackbar: false,
      errorSnackbar: false,
      successSnackbar: false,
      snackbarColor: ''
    };
  },
  methods: {
    validateBeforeSubmit: function validateBeforeSubmit() {
      if (this.$refs.form.validate() == true) {
        this.save();
      } else {
        this.snackbarColor = "red";
        this.errors[0] = "Please check the form for errors.";
        this.errorSnackbar = true;
        this.snackbar = true;
      }
    },
    save: function save() {
      var _this = this;

      axios.post('/api/locationDesperation', this.userItem)["catch"](function (error) {
        if (error.response.status == 422) {
          _this.newErrors = error.response.data.errors;
          _this.newErrors = Object.values(_this.newErrors);

          for (var i = 0; i < _this.newErrors.length; i++) {
            _this.errors[i] = _this.newErrors[i][0].replace('title.' + i, i + 1);
          }

          _this.snackbarColor = "red";
          _this.errorSnackbar = true;
          _this.snackbar = true;
        }
      }).then(function (response) {
        _this.response = response.data;
        _this.snackbarColor = "green";
        _this.successSnackbar = true;
        _this.snackbar = true;

        _this.$refs.form.reset();
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/HomeContent.vue?vue&type=template&id=31091aa1&":
/*!*********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/HomeContent.vue?vue&type=template&id=31091aa1& ***!
  \*********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "v-container",
        [
          _c("v-row", { staticClass: "justify-center mt-8" }, [
            _c("h1", [_vm._v(" JFD - New Developer Test ")])
          ]),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c("v-col", { attrs: { cols: "12" } }, [
                _vm._v(
                  "\n\t\t\t\tLocal lockdowns are preventing people going out. We wish to capture the first place that\n\t\t\t\tyou are going to go when you are finally allowed out and how desperate you are to go there. Please fill the form out below including the location you will first visit and a desperation rating from 1 to 5, 1 being not desperate at all and 5 being extremely desperate.\n\t\t\t"
                )
              ]),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "12" } },
                [
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-form",
                        {
                          ref: "form",
                          staticStyle: { width: "100%" },
                          model: {
                            value: _vm.form,
                            callback: function($$v) {
                              _vm.form = $$v
                            },
                            expression: "form"
                          }
                        },
                        [
                          _c(
                            "v-col",
                            { attrs: { cols: "12" } },
                            [
                              _c("v-select", {
                                attrs: {
                                  items: _vm.locations,
                                  label: "Location",
                                  rules: [_vm.rules.required]
                                },
                                model: {
                                  value: _vm.userItem.location,
                                  callback: function($$v) {
                                    _vm.$set(_vm.userItem, "location", $$v)
                                  },
                                  expression: "userItem.location"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-col",
                            { attrs: { cols: "12" } },
                            [
                              _c("v-textarea", {
                                attrs: {
                                  label: "Comment",
                                  rows: "5",
                                  counter: "300",
                                  rules: [_vm.rules.comment_count]
                                },
                                model: {
                                  value: _vm.userItem.comment,
                                  callback: function($$v) {
                                    _vm.$set(_vm.userItem, "comment", $$v)
                                  },
                                  expression: "userItem.comment"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-col",
                            { attrs: { cols: "12" } },
                            [
                              _vm._v(
                                "\n\t\t            \t\tDesperation Rating:\n\t\t            \t\t"
                              ),
                              _c(
                                "v-radio-group",
                                {
                                  attrs: {
                                    column: "",
                                    rules: [_vm.rules.required]
                                  },
                                  model: {
                                    value: _vm.userItem.desperation,
                                    callback: function($$v) {
                                      _vm.$set(_vm.userItem, "desperation", $$v)
                                    },
                                    expression: "userItem.desperation"
                                  }
                                },
                                [
                                  _c("v-radio", {
                                    attrs: {
                                      label: "1 - Not Desperate",
                                      value: "1"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("v-radio", {
                                    attrs: { label: "2", value: "2" }
                                  }),
                                  _vm._v(" "),
                                  _c("v-radio", {
                                    attrs: { label: "3", value: "3" }
                                  }),
                                  _vm._v(" "),
                                  _c("v-radio", {
                                    attrs: { label: "4", value: "4" }
                                  }),
                                  _vm._v(" "),
                                  _c("v-radio", {
                                    attrs: {
                                      label: "5 - Extremely Desperate",
                                      value: "5"
                                    }
                                  })
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-col",
                            { attrs: { cols: "12" } },
                            [
                              _c(
                                "v-btn",
                                {
                                  attrs: {
                                    tile: "",
                                    block: "",
                                    "x-large": "",
                                    color: "blue"
                                  },
                                  on: {
                                    click: function($event) {
                                      return _vm.validateBeforeSubmit()
                                    }
                                  }
                                },
                                [_vm._v(" Submit ")]
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-snackbar",
        {
          attrs: { color: _vm.snackbarColor, width: "600" },
          scopedSlots: _vm._u([
            {
              key: "action",
              fn: function(ref) {
                var attrs = ref.attrs
                return [
                  _c(
                    "v-btn",
                    _vm._b(
                      {
                        attrs: { color: "white", text: "" },
                        on: {
                          click: function($event) {
                            _vm.snackbar = false
                          }
                        }
                      },
                      "v-btn",
                      attrs,
                      false
                    ),
                    [_vm._v("\n          Close\n        ")]
                  )
                ]
              }
            }
          ]),
          model: {
            value: _vm.snackbar,
            callback: function($$v) {
              _vm.snackbar = $$v
            },
            expression: "snackbar"
          }
        },
        [
          _vm._l(_vm.errors, function(error) {
            return _vm.errorSnackbar
              ? _c("v-row", [_vm._v("\n      \t" + _vm._s(error) + "\n      ")])
              : _vm._e()
          }),
          _vm._v(" "),
          _vm.successSnackbar
            ? _c("v-row", [
                _vm._v("\n      \t" + _vm._s(_vm.response) + "\n      ")
              ])
            : _vm._e()
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/pages/HomeContent.vue":
/*!********************************************!*\
  !*** ./resources/js/pages/HomeContent.vue ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _HomeContent_vue_vue_type_template_id_31091aa1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./HomeContent.vue?vue&type=template&id=31091aa1& */ "./resources/js/pages/HomeContent.vue?vue&type=template&id=31091aa1&");
/* harmony import */ var _HomeContent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./HomeContent.vue?vue&type=script&lang=js& */ "./resources/js/pages/HomeContent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _HomeContent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _HomeContent_vue_vue_type_template_id_31091aa1___WEBPACK_IMPORTED_MODULE_0__["render"],
  _HomeContent_vue_vue_type_template_id_31091aa1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/HomeContent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/pages/HomeContent.vue?vue&type=script&lang=js&":
/*!*********************************************************************!*\
  !*** ./resources/js/pages/HomeContent.vue?vue&type=script&lang=js& ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeContent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./HomeContent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/HomeContent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeContent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/pages/HomeContent.vue?vue&type=template&id=31091aa1&":
/*!***************************************************************************!*\
  !*** ./resources/js/pages/HomeContent.vue?vue&type=template&id=31091aa1& ***!
  \***************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeContent_vue_vue_type_template_id_31091aa1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./HomeContent.vue?vue&type=template&id=31091aa1& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/HomeContent.vue?vue&type=template&id=31091aa1&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeContent_vue_vue_type_template_id_31091aa1___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeContent_vue_vue_type_template_id_31091aa1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);