/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

window.Vue = require('vue');

import VueRouter from 'vue-router'
import Vue from 'vue'
import vuetify from 'laravel-mix/src/webpackPlugins/vuetify' // path to vuetify export
import axios from 'axios';

window.axios = axios;

Vue.use(VueRouter)



/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


Vue.component('admin', require('./components/Admin.vue').default);
Vue.component('index', () => import('./components/Index.vue'));

let Dashboard = () => import ('./pages/Dashboard');
let Home = () => import ('./pages/HomeContent');


/** Defining the routes for vue-router */

const routes = [
{
		path: '/',
		component: Home
},

{
		path: '/admin/',
		component: Dashboard
},

];

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/** Allow anchor tags to be used within vue-router & ensure pages navigate to the top rather than where the route page position was */
var router = new VueRouter({

  scrollBehavior: function(to, from, savedPosition) {
    if (to.hash) {
      return {
	      selector: to.hash	      
      }
    } else {
      return {x: 0, y: 0}
    }
  },
  routes: routes,
  mode: 'history'
});

/** Initialize a new vue instance */
const app = new Vue({
  el: '#app',
  router: router,
  vuetify, 
});
